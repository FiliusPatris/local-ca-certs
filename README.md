# Generating local Certificate Authority and signing certificates

## Use cases
You might want to encrypt traffic from your private server or in your local network.
Getting a fully accepted certificate can be a hassle (and before Let's Encrypt even costly),
so you might want to just create these certificates for yourself.


**Outdated**: DM me if you run into problems

## Usage

- Run `make` in your terminal in this directory. Enter the same password twice (between 4 and 1023 characters).
- Trust the `ca.pem` cert:
  - MacOS: Import into keychain by double-clicking. View Details > Trust. Switch to "Always Trust".
  - Linux: It's pretty much the problem of every single application: [Unix SE question](https://unix.stackexchange.com/questions/90450/adding-a-self-signed-certificate-to-the-trusted-list)
  - Windows: Something with group policy management. TechRepublic seems to have a [solid guide](https://www.techrepublic.com/article/how-to-add-a-trusted-certificate-authority-certificate-to-internet-explorer-or-microsoft-edge/) not tested though.
  - Some browsers and programs have their own CA database. Search the internet for guides...
- Use the `cert.crt` certificate and the `cert.key` private key in your webserver.

## Basic workflow
This outlines the manual steps neccesary to make it work. See also the `gen-ca.sh` and `gen-cert.sh` files.

### Creating a local Certificate Authority (CA)
First generate a private key (to the file `ca.key`):
```sh
openssl genrsa -out ca.key
```

Next generate it's certificate. This certificate won't be signed, since it is the root ceriticate.
It will be trusted by the user manually saying so.
The extended configs are written in `ca.conf` You can edit them or remove them at all and provide the
options via commandline arguments. (Lookup the manpage for details.)
```sh
openssl req -x509 -new -nodes -key ca.key -sha256 -days 1825 -config ca.conf -out ca.crt
```

### Creating and signing a certificate
First let's again generate a private key (this time `cert.key`):
```sh
openssl genrsa -out cert.key
```

Next we'll create a *Certificate Sing Request* (CSR) for the CA. With that they'll be able to create
a signed certificate. It will be written to the file `cert.csr`.
Again, many of the values are written in `csr.conf`. Edit or remove it if you want.
```sh
openssl req -new -config csr.conf -out cert.csr
```

Finally, sign the CSR to get a certificate. This would be done by the CA, but since we are both
sides at the same time, we can do it ourselves (and that's the entire point of a local CA).
This will require the CSR, of course and the private CA key. The destination file (the certificate)
```sh
openssl x509 -req -in cert.csr -CA ca.pem -CAkey ca.key -CAcreateserial -days 825 -sha256 -out cert.crt
```
You might notice that it also creates the `ca.srl` file. CAs insert a serial numbe into the certificate
and keep this also as file.

## Configuration
Check out the openssl manpage: `man openssl`.

## Acknowledgments
Thanks a log to [this blogpost](https://deliciousbrains.com/ssl-certificate-authority-for-local-https-development/)
by Delicious Brains.