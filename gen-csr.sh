#!/bin/sh

# Generate private key and CSR
#   -newkey <alg>  generate a new private key if needed
openssl req -config gen-csr.conf -newkey rsa:2048 -nodes -out server_csr.csr