#!/bin/sh

openssl ca -config sign-csr.conf -policy signing_policy -extensions signing_req -out server_cert.pem -infiles server_csr.csr