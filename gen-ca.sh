#!/bin/sh

# Generate the CA:
#   -x509			means it's a root certificate (otherwhise it'll output a CSR)
#   -newkey <alg>	generates a private key (location in gen-ca.conf)
openssl req -x509 -config gen-ca.conf -newkey rsa:4096 -out ca_cert.pem;

# Setup workspace

# Index of all signed certificates
touch index.txt;
# File in which serial numbers are counted
echo '01' > serial.txt;